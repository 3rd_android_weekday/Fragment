package com.kshrd.fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.kshrd.fragment.fragment.AFragment;
import com.kshrd.fragment.fragment.BFragment;

public class DynamicFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ts = fm.beginTransaction();

        if (fm.findFragmentByTag("ARTICLE_FRAGMENT") == null) {
            AFragment aFragment = new AFragment();
            ts.add(R.id.container, aFragment, "ARTICLE_FRAGMENT");
            ts.commit();
        }

        findViewById(R.id.btnReplace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm1 = getSupportFragmentManager();
                FragmentTransaction ts1 = fm1.beginTransaction();
                ts1.replace(R.id.container, new BFragment(), "ARTICLE_FRAGMENT");
                ts1.addToBackStack(null);
                ts1.commit();
            }
        });


    }
}
