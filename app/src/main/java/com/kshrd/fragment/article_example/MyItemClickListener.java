package com.kshrd.fragment.article_example;

/**
 * Created by pirang on 6/2/17.
 */

public interface MyItemClickListener {

    void onItemClick(String brand);

}
