package com.kshrd.fragment.article_example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.fragment.R;
import com.kshrd.fragment.fragment.DetailFragment;

public class ArticleActivity extends AppCompatActivity implements MyItemClickListener{

    DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.detailFragment);

    }

    @Override
    public void onItemClick(String brand) {
        if (detailFragment != null){
            detailFragment.updateText(brand);
        }
    }
}
