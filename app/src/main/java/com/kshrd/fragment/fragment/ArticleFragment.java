package com.kshrd.fragment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kshrd.fragment.R;
import com.kshrd.fragment.article_example.MyItemClickListener;

/**
 * Created by pirang on 6/2/17.
 */

public class ArticleFragment extends Fragment {

    MyItemClickListener myItemClickListener;

    String[] list = {"Apple", "Samsung", "Honda", "Suzuki", "Yamaha", "KTM"};

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MyItemClickListener){
            myItemClickListener = (MyItemClickListener) context;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView) view.findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_list_item_1, list
        );

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (myItemClickListener != null){
                    myItemClickListener.onItemClick(list[position]);
                }
            }
        });

    }
}
