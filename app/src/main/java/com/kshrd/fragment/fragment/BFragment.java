package com.kshrd.fragment.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kshrd.fragment.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BFragment extends Fragment {

    TextView tvDetail;


    public BFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_b, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvDetail = (TextView) view.findViewById(R.id.tvDetail);
    }

    public void updateDetail(String value){
        tvDetail.setText(value);
    }
}
