package com.kshrd.fragment;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kshrd.fragment.fragment.BFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        BFragment bFragment = (BFragment) fm.findFragmentById(R.id.bFragment);
        bFragment.updateDetail("New Value from Activity");

    }
}
